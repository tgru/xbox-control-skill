# <img src='https://rawgithub.com/FortAwesome/Font-Awesome/master/advanced-options/raw-svg/solid/gamepad.svg' card_color='#22a7f0' width='50' height='50' style='vertical-align:bottom'/> Xbox Control

Lets you control your Xbox One by voice.

## Description

This is a skill for the Mycroft AI voice assistant, which enables you to control your Xbox One with voice commands.

## Installation

At the moment this skill requires an [Xbox Smartglass REST server](https://github.com/OpenXbox/xbox-smartglass-rest-python) from OpenXbox running somewhere. In case the server is not running on the same device the URL and port of this server can be configured in the skill settings, since the default setting is `localhost:5557`.

Apart from this, a normal skill installation by cloning this repository can be done.

## Examples

 - "Power on the Xbox"
 - "Switch the Xbox off"

## Credits

@tgru

## Disclaimer

Xbox, Xbox One, Smartglass and Xbox Live are trademarks of Microsoft Corporation. This project is in no way endorsed by or affiliated with Microsoft Corporation, or any associated subsidiaries, logos or trademarks.